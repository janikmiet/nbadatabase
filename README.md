# Stattleship to MariaDB SQL database

**NO MORE STATTLESHIP SUPPORT! THIS PROJECT IS DEAD.**


Program downloads data from Stattleship API to local sql-database. Install this project to your server side.

## Supported sports

* NBA

## supported tables

* GAMES
* GAME_LOGS
* PLAYERS
* PLAYER_SEASON_STATS
* INJURIES
* TEAMS
* ROSTERS
* TEAM_GAME_LOGS
* TEAM_SEASON_STATS
* OFFICIALS
* SEASONS


## Installation guide


1. Install R and packages

```r
devtools::install_github("stattleship/stattleship-r")
install.packages("RMariaDB")
```

2. **Create database.** Grant permissions and ssh-connection to MariaDB database (*CREATE USER* and *GRANT* permissions for ex. user `DBADMIN`)
 
Create user `DBADMIN` with all priviledges to database:

```sql
CREATE USER 'DBADMIN'@'*' IDENTIFIED BY 'mysuper1password';
CREATE DATABASE STATTLESHIP;
GRANT ALL PRIVILEGES ON STATTLESHIP.* TO DBADMIN@'*';
FLUSH PRIVILEGES;
```

Create/Grant `USER1` with SELECT privileges to database:

```sql
GRANT SELECT ON STATTLESHIP.* TO 'USER1'@'*' IDENTIFIED BY 'myuserpassword';;
FLUSH PRIVILEGES;
```

3. **Setup and automate program.** 

You need to create to files for passwords

```r
file.edit("_database_password.txt") # for admin password to database
file.edit("_token.txt") # for Stattleship token
```

Modify `run.R` script with your credentials and run script to copy files to server.

```r
file.edit("run.R")
# source("run.R")
```

* `init_database = TRUE`        Initialize database with 'CREATE TABLE' DDL-scripts.
* `baseload_database = TRUE`    Make baseload. If false, updates tables with 'UPDATE_AT' field.


4. **Final touch.** Automate script in server side with `crontab -e` adding this

```
0 8,21 * * * cd /nbadatabase/ ; Rscript run.R
```

## More information

- Stattleship <https://api.stattleship.com/>
- Stattleship API documentation: <http://developers.stattleship.com/>
 
 

### TODO

# create play by play update script


1. ERROR IF NOT SUCCESFUL

2. PLAY_BY_PLAY
- Create TABLE script
- DWL SCRIPT
- UPD SCRIPT


3. SCOREBOARDS
- DWL SCRIPT
- UPD SCRIPT


4. FEATS
- DWL SCRIPT
- UPD SCRIPT

 
<br>
<br> (C) Jani Miettinen
<br> Licence: MIT

