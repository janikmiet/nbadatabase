DROP TABLE IF EXISTS PLAYERS;
CREATE TABLE `PLAYERS` (
  `ID` CHAR (36) NOT NULL PRIMARY KEY,
  `CREATED_AT` timestamp,
  `UPDATED_AT` timestamp,
  `ACTIVE` tinyint(4) DEFAULT NULL,
  `BATS` text,
  `BIRTH_DATE` date,
  `CAPTAIN` tinyint(4) DEFAULT NULL,
  `CITY` text,
  `COUNTRY` text,
  `DRAFT_OVERALL_PICK` int(11) DEFAULT NULL,
  `DRAFT_ROUND` int(11) DEFAULT NULL,
  `DRAFT_SEASON` text,
  `DRAFT_TEAM_NAME` text,
  `FIRST_NAME` text,
  `HANDEDNESS` text,
  `HEIGHT` int(11) DEFAULT NULL,
  `HIGH_SCHOOL` tinyint(4) DEFAULT NULL,
  `HUMANIZED_SALARY` text,
  `LAST_NAME` text,
  `MLBAM_ID` tinyint(4) DEFAULT NULL,
  `NAME` text,
  `NICKNAME` text,
  `POSITION_ABBREVIATION` text,
  `POSITION_NAME` text,
  `PRO_DEBUT` tinyint(4) DEFAULT NULL,
  `SALARY` int(11) DEFAULT NULL,
  `SALARY_CURRENCY` text,
  `SCHOOL` text,
  `SLUG` text,
  `SPORT` text,
  `STATE` text,
  `UNIFORM_NUMBER` text,
  `UNIT_OF_HEIGHT` text,
  `UNIT_OF_WEIGHT` text,
  `WEIGHT` int(11) DEFAULT NULL,
  `YEARS_OF_EXPERIENCE` int(11) DEFAULT NULL,
  `LEAGUE_ID` CHAR (36),
  `PLAYING_POSITION_ID` CHAR (36),
  `TEAM_ID` CHAR (36)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
