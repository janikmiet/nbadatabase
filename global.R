# Global options and functions
library(stattleshipR, quietly = T, warn.conflicts = F)
library(tidyverse, quietly = T, warn.conflicts = F)
library(RMariaDB, quietly = T, warn.conflicts = F)
library(DBI, quietly = T, warn.conflicts = F)
library(dbtools)
options(stringsAsFactors=FALSE)
set_token(readLines("_token.txt"))

# Functions ---------------

find_duplicates <- function(group=database_name, delete=FALSE){
  con <- DBI::dbConnect(RMariaDB::MariaDB(), group = group)
  tables <- DBI::dbListTables(con)
  for(TABLE in tables){
    # Jatketaan jos loytyy ID 
    if("ID" %in% DBI::dbListFields(con, TABLE)){
      ID <- DBI::dbGetQuery(con, paste0("SELECT DISTINCT ID, COUNT(*) TUPLAT FROM ", TABLE, " GROUP BY ID;"))
      ID <- ID[ID$TUPLAT>1, "ID"]
      # Jatketaan jos on tuplia
      if(nrow(ID)>0){
        d <- DBI::dbGetQuery(con, paste0("SELECT * FROM ", TABLE, " WHERE ID IN ('", paste0(ID, collapse = "', '"), "');"))
        d[duplicated(d), "ID"]
        # TODO HOW TO KEEP ONLY ONE ROW AND DELETE REST?
      }
    }
  }
  DBI::dbDisconnect(con)
}

uploadTable <- function(object, 
                      group = database_name, 
                      overwrite = F, 
                      append = T){
  filename <- deparse(substitute(object)) # Filename
  msg <- paste0(Sys.time(), " Writing ", filename, " (", nrow(object) ," rows) to ", group)
  print(msg)
  con <- dbConnect(RMariaDB::MariaDB(), group = group)
  dbWriteTable(con, filename, object, overwrite=overwrite, append=append)
  dbDisconnect(con)
  msg <- paste0(Sys.time(), " Connection to MariaDB closed.") # Log and msg
  print(msg)
}

updateTable <- function(object, group = database_name){
  overwrite = F
  append = T
  filename <- toupper(deparse(substitute(object))) # Filename
  # Log and msg
  msg <- paste0(Sys.time(), " ", filename, " Updating ", nrow(object) ," rows to ", group)
  print(msg)
  # dbconnection
  con <- dbConnect(RMariaDB::MariaDB(), group = group)
  # delete records
  sql_query <- paste0("DELETE FROM ", filename, " WHERE ID IN ('", paste0(object$ID, collapse = "', '"),"');")
  dbSendQuery(con, sql_query)
  dbWriteTable(con, filename, object, overwrite=overwrite, append=append)
  dbDisconnect(con)
  msg <- paste0(Sys.time(), " Connection to MariaDB closed.")
  print(msg)
}

metadata_update <- function(dataframe, 
                            action = "UPDATE", 
                            sport="NBA",
                            since=NA, 
                            slug=NA, 
                            interval_type = NA,
                            group = database_name){
  con <- DBI::dbConnect(RMariaDB::MariaDB(), group = database_name)
  TIME = Sys.time()
  TABLE = deparse(substitute(dataframe))
  ACTION = action
  SPORT=sport
  SINCE = as.Date(since)
  SLUG=slug
  INTERVAL_TYPE=interval_type
  ROWS_AFFECTED = nrow(dataframe)
  COLS_AFFECTED = ncol(dataframe)
  NOTE = ""
  METADATA <- data.frame(TIME, TABLE, ACTION, SPORT, SINCE, SLUG, INTERVAL_TYPE, ROWS_AFFECTED, COLS_AFFECTED, NOTE)
  dbWriteTable(con, "UPDATELOG", METADATA, overwrite=FALSE, append=TRUE)
}


get_seasons <- function(league="NBA"){
  hockey_seasons <- c("nhl-2015-2016", "nhl-2016-2017", "nhl-2017-2018")
  basketball_seasons <- c("nba-2015-2016", "nba-2016-2017", "nba-2017-2018")
  if(league == "nhl") {
    seasons = hockey_seasons
  }
  
  if(league == "nba") {
    seasons = basketball_seasons
  } 
  return(seasons)
}
get_intervals <- function(league="NBA"){
  hockey_interval_types <- c("allstar",
                             "conferencequarterfinals",
                             "postponed",
                             "preseason",
                             "regularseason",
                             "conferencequarterfinals",
                             "conferencesemifinals",
                             "conferencefinals",
                             "stanleycupfinals")
  basketball_interval_types <- c("allstar",
                                 "conferencequarterfinals",
                                 "conferencesemifinals",
                                 "conferencefinals",
                                 "nbachampionship",
                                 "postponed",
                                 "preseason",
                                 "regularseason",
                                 "risingstarschallenge")
  
  if(league == "nhl") {
    interval_types = hockey_interval_types
  }
  
  if(league == "nba") {
    interval_types = basketball_interval_types
  }
  return(interval_types)
}



dwl_game_logs <- function(player_id="nba-lauri-markkanen", season_id = "nba-2018-2019", interval_type = "regularseason"){
  database_name = "STATTLESHIP_ADMIN"
  q_body <- list(player_id=player_id, season_id = season_id, interval_type = interval_type)
  tmp <- ss_get_result(sport="basketball", 
                       league="nba", 
                       ep="game_logs", 
                       query=q_body, 
                       verbose=TRUE, 
                       walk = TRUE) 
  GAME_LOGS <- do.call("rbind", lapply(tmp, function(x) x$game_logs))
  names(GAME_LOGS) <- toupper(names(GAME_LOGS))
  GAME_LOGS <- GAME_LOGS[!duplicated(GAME_LOGS), ] # ERASES DUPLICATE ROWS!   
  updateTable(object = GAME_LOGS, group = "STATTLESHIP_ADMIN")
  metadata_update(dataframe = GAME_LOGS, action = "MANUAL LOAD", since = NA, slug = season_id, interval_type=interval_type)
}

# Variables -----

## SEASON SLUG  -----
seasonslug <- get_query("SELECT SLUG FROM SEASONS ORDER BY STARTS_ON DESC LIMIT 1; ")[[1]]

# Script end message -----------
print(paste0(Sys.time(), " Loaded global.R script."))
