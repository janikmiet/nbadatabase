# Stattleship API download all GAMES for season, interval_type
# ------------------------------------------------------------
league <- "nba"
sport <- "basketball"
ep <- "officials" 
since <- as.Date(get_query("SELECT MAX(UPDATED_AT) FROM OFFICIALS;")[[1]])
q_body <- list(since=since)
msg <- paste0(Sys.time(), " ", toupper(ep)," Checking data since: ", since)
print(msg)
tmp <- ss_get_result(sport = sport, 
                     league = league, 
                     ep = ep, 
                     query = q_body, 
                     version = 1, 
                     walk = TRUE)
OFFICIALS <- do.call("rbind", lapply(tmp, function(x) x$officials))
names(OFFICIALS) <- toupper(names(OFFICIALS))
# Upload data to MDB
if(is.data.frame(OFFICIALS)){
  # saveRDS(OFFICIALS , file = "./data/OFFICIALS.RDS")
  updateTable(OFFICIALS)
  metadata_update(dataframe = OFFICIALS, action = "UPDATE", since = since)
}
