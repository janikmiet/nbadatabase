# Stattleship API download all GAMES for season, interval_type
# ------------------------------------------------------------
league <- "nba"
sport <- "basketball"
ep <- "games"
since <- as.Date(get_query("SELECT MAX(UPDATED_AT) FROM GAMES;")[[1]])
# KYSELY SINCE PERUSTEELLA
msg <- paste0(Sys.time(), " ", toupper(ep)," Checking data since: ", since)
print(msg)
q_body <- list(since=since)
tmp <- ss_get_result(sport=sport, 
                     league=league, 
                     ep=ep, 
                     query=q_body, 
                     # version=1,
                     verbose=TRUE, 
                     walk = TRUE) 
GAMES <- do.call("rbind", lapply(tmp, function(x) x$games))
rm(tmp)
if(is.data.frame(GAMES)){
  msg <- paste0(Sys.time(), " ", toupper(ep), " Received a data frame with ", nrow(GAMES), " rows and ", ncol(GAMES), " columns.")
  print(msg)
  GAMES$official_ids <- as.character(GAMES$official_ids)
  names(GAMES) <- toupper(names(GAMES))
  if(REMOVE_DUPLICATES){
    GAMES <- GAMES[!duplicated(GAMES), ] # ERASES DUPLICATE ROWS!
  }
  # UPDATE TO DATABASE
  if(TRUE){
    updateTable(GAMES)
    metadata_update(dataframe = GAMES, action = "UPDATE", since = since)
  }
} else{
  msg <- paste0(Sys.time(), " ", toupper(ep), " No new data.")
  print(msg)
}
