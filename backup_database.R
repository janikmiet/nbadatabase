# Backup of the database
source("global.R")
dbname <- gsub("_ADMIN","",database_name)
msg(paste0(" backup job ", dbname," started..."))
cmd <- paste0("mysqldump --user=",database_user," --password=",database_password," ", dbname," > ./backup/backup_", dbname, "_", Sys.Date(), ".sql")
system(cmd)
msg(paste0(" backup job ", dbname," finished."))
