---
title: "database report"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
source("global.R")
# get tablenames
con <- odbc::dbConnect(RMariaDB::MariaDB(), group="STATTLESHIP")

# db_tbls <- odbc::dbListTables(con)
# odbc::dbListFields(con, "CONFERENCES")

odbc::dbDisconnect(con)
```

## GAMES


```{r}
get_query("SELECT MAX(ENDED_AT) AS Last_GAME FROM GAMES  WHERE AWAY_TEAM_OUTCOME <> 'undecided';")
```


